﻿using PaperRolez.Interfaces;
using PaperRolez.Process;
using PaperRolez.Repo;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperRolez.BLL
{
    public class Process
    {
        private readonly IDocumentAccess _documentAccess;
        private readonly ILookupStore _documentStore;

        public Process(IDocumentAccess documentAccess, ILookupStore documentStore)
        {
            _documentAccess = documentAccess;
            _documentStore = documentStore;
        }

        public void DailyProcessment(string source, string client, string documentId, string documentName)
        {

            //1: Load a document to work
            MemoryStream fileData = _documentAccess.Load(source, client, documentId, documentName);

            //2: Identify the kind of processment
            StreamReader sr = new StreamReader(fileData);
            String processingType = sr.ReadLine().Split('|')[0];
            List<String> parameters = sr.ReadLine().ToLower().Split(',').ToList();

            switch (processingType)
            {
                case "lookup":
                    //3: Process the document
                    var result = ProcessMethods.Lookup(fileData, parameters);

                    //4: Store the result
                    _documentStore.Record(client, documentId, parameters);
                    break;

                case "find":
                    //further process
                    //var bla = ProcessMethods.find(...);
                    break;

                case "default":
                    //many options
                    //throw new NotImplementedException();
                    break;
            }

            //5: Delete the document
            _documentAccess.Remove(source, client, documentId, documentName);
        }

    }
}
