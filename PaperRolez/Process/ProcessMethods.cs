﻿using PaperRolez.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperRolez.Process
{
    public static class ProcessMethods
    {
        public static IEnumerable<string> Lookup(MemoryStream data, List<string> parameters)
        {
            List<String> result = new List<string>();

            using (var sr = new StreamReader(data))
            {
                String line = sr.ReadToEnd().ToLower();

                foreach (var word in parameters)
                {
                    if (line.Contains(word))
                        result.Add(word);
                }
            }
            return result;
        }
    }
}
