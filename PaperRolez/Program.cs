﻿using Ninject;
using PaperRolez.BLL;
using PaperRolez.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace PaperRolez
{
    class Program
    {
        static void Main(string[] args)
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());

            //Ninject
            var docAcces = kernel.Get<IDocumentAccess>();
            var docStore = kernel.Get<ILookupStore>();

            BLL.Process pcs = new BLL.Process(docAcces, docStore);

            //Data from other part of program...
            String source = "C:\\";
            String client = "SlsOptmze";
            String documentId = "97d18c72-7343-46b7-b6cb-3366349cb357";
            String documentName = "JobSpecs.txt";

            pcs.DailyProcessment(source, client, documentId, documentName);
        }
    }
}
