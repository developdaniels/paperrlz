﻿using PaperRolez.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PaperRolez.Repo
{
    /// <summary>
    /// Class which load and remove file to a ftp server
    /// </summary>
    class FtpAccess : IDocumentAccess
    {
        public MemoryStream Load(string source, string client, string documentId, string documentName)
        {
            WebClient request = new WebClient();

            //NamingConvention: documentId_documentName
            String fileName = String.Format("{0}_{1}", documentId, documentName);
            Uri fileUri = new Uri(new Uri(source), String.Format("{0}/{1}", client, documentId));
            
            MemoryStream memStream = new MemoryStream();
            try
            {
                using (var webFile = request.OpenRead(fileUri))
                {
                    webFile.CopyTo(memStream);
                    webFile.Close();
                    return memStream;
                }
            }
            catch (WebException e)
            {
                //MANY option here...
                //throw...
                return (memStream);
            }
            catch (Exception e) {
                //MANY option here...
                //throw...
                return (memStream);
            }
        }

        public void Remove(string source, string client, string documentId, string documentName)
        {
            //NamingConvention: documentId_documentName
            String fileName = String.Format("{0}_{1}", documentId, documentName);
            Uri fileUri = new Uri(new Uri(source), String.Format("{0}/{1}", client, fileName));

            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(fileUri);
                request.Method = WebRequestMethods.Ftp.DeleteFile;

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                response.Close();
            }
            catch (WebException e)
            {
                //MANY option here...
                //throw...
            }
            catch (Exception e)
            {
                //MANY option here...
                //throw...
            }
        }

    }
}
