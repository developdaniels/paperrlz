﻿using PaperRolez.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperRolez.Repo
{
    /// <summary>
    /// Class which load, remove and store a file to a disk drive
    /// </summary>
    class FileAccess : IDocumentAccess, IDocumentStore
    {
        //Load file from disk
        public MemoryStream Load(string path, string client, string documentId, string documentName)
        {
            //NamingConvention: documentId_documentName
            String fileName = String.Format("{0}_{1}", documentId, documentName);

            //eg, C:\\client\fileName
            String fullPath = Path.Combine(path, client, fileName);

            if (Directory.Exists(fullPath))
            {
                MemoryStream memStream = new MemoryStream();

                using (var fileStream = File.OpenRead(fullPath))
                {
                    fileStream.CopyTo(memStream);
                }

                return memStream;
            }
            else
            {
                //MANY options here...
                //throw new FileNotFoundException();
                //return null;
                return new MemoryStream();
            }
        }

        //Remove file from disk
        public void Remove(string path, string client, string documentId, string documentName)
        {
            //NamingConvention: documentId_documentName
            String fileName = String.Format("{0}_{1}", documentId, documentName);

            //eg, C:\\client\fileName
            String fullPath = Path.Combine(path, client, fileName);

            if (Directory.Exists(fullPath))
            {
                File.Delete(fullPath);
            }
            else
            {
                throw new FileNotFoundException();
            }
        }

        //Save file to disk
        public void Store(MemoryStream file, string path, string client, string documentId, string documentName)
        {
            //NamingConvention: documentId_documentName
            String fileName = String.Format("{0}_{1}", documentId, documentName);

            //eg, C:\\client
            String basePath = Path.Combine(path, client);

            if (Directory.Exists(basePath))
            {
                String fullPath = Path.Combine(basePath, fileName);
                
                using (var newFile = File.Create(fullPath))
                {
                    file.CopyTo(newFile);
                    newFile.Close();
                }
            }
            else
            {
                //MANY options here...
                throw new FileNotFoundException();
            }
        }
    }
}
