﻿using PaperRolez.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperRolez.Repo
{
    /// <summary>
    /// Class which store a Document into SQL Server table
    /// </summary>
    class SqlAccess : IDocumentStore
    {
        private readonly SqlConnection _sqlConn;

        public SqlAccess(SqlConnection sqlConn)
        {
            _sqlConn = sqlConn;
        }

        public void Store(MemoryStream file, string source, string client, string documentId, string documentName)
        {
            //NamingConvention: documentId_documentName
            String fileName = String.Format("{0}_{1}", documentId, documentName);
            Uri fileUri = new Uri(new Uri(source), String.Format("{0}/{1}", client, fileName));

            //insert the file into database
            string strQuery = "insert into tblFiles(Name, ContentType, Data) values (@Name, @Data)";

            SqlCommand cmd = new SqlCommand(strQuery);

            cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = fileName;
            cmd.Parameters.Add("@Data", SqlDbType.Binary).Value = file.ToArray();

            cmd.Connection = _sqlConn;
            //continue...

        }
    }
}
