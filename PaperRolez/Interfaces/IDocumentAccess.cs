﻿using System.IO;

namespace PaperRolez.Interfaces
{
    /// <summary>
    /// Interface to load and remove a document
    /// </summary>
    public interface IDocumentAccess
    {
        
        /// <param name="source">Parent source of documents</param>
        /// <param name="client">Client identifier</param>
        /// <param name="documentId">Document identifier</param>
        /// <param name="documentName">Document name</param>
        MemoryStream Load(string source, string client, string documentId, string documentName);

        /// <param name="source">Parent source of documents</param>
        /// <param name="client">Client identifier</param>
        /// <param name="documentId">Document identifier</param>
        /// <param name="documentName">Document name</param>
        void Remove(string source, string client, string documentId, string documentName);
    }
}
