﻿using System.IO;


namespace PaperRolez.Interfaces
{
    /// <summary>
    /// Interface to save a file to some destination
    /// </summary>
    public interface IDocumentStore
    {

        /// <param name="file">Data to be saved</param>
        /// <param name="source">Path of parent documents folder</param>
        /// <param name="client">Client identifier</param>
        /// <param name="documentId">Document identifier</param>
        /// <param name="documentName">Document name</param>
        void Store(MemoryStream file, string source, string client, string documentId, string documentName);
    }

}
