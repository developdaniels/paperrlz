﻿using Ninject.Modules;
using Ninject;
using PaperRolez.Repo;
using PaperRolez.Interfaces;

namespace PaperRolez
{
    public class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IDocumentAccess>().To<FileAccess>();
            Bind<IDocumentStore>().To<SqlAccess>();
            Bind<ILookupStore>().To<LookupStore>();
        }
    }
}
